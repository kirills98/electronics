program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  dlg in 'dlg.pas' {OKRightDlg},
  Event in 'Event.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TOKRightDlg, OKRightDlg);
  Application.Run;
end.
