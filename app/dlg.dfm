object OKRightDlg: TOKRightDlg
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Dialog'
  ClientHeight = 179
  ClientWidth = 384
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 281
    Height = 161
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 24
    Top = 27
    Width = 49
    Height = 13
    Caption = #1058#1080#1087' '#1089#1074#1103#1079#1080
  end
  object Label2: TLabel
    Left = 24
    Top = 54
    Width = 50
    Height = 13
    Caption = '1 '#1101#1083#1077#1084#1077#1085#1090
  end
  object Label3: TLabel
    Left = 24
    Top = 81
    Width = 50
    Height = 13
    Caption = '2 '#1101#1083#1077#1084#1077#1085#1090
  end
  object OKBtn: TButton
    Left = 300
    Top = 8
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 300
    Top = 38
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object ComboBox1: TComboBox
    Left = 136
    Top = 24
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    Items.Strings = (
      'CtInput'
      'CtOutput')
  end
  object ComboBox2: TComboBox
    Left = 136
    Top = 51
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 3
  end
  object ComboBox3: TComboBox
    Left = 136
    Top = 78
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 4
  end
end
