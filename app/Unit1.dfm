object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 636
  ClientWidth = 735
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClick = FormClick
  OnCreate = FormCreate
  OnMouseMove = FormMouseMove
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 429
    Top = 29
    Height = 607
    Align = alRight
    ExplicitLeft = 376
    ExplicitTop = 288
    ExplicitHeight = 100
  end
  object ListBox1: TListBox
    Left = 0
    Top = 29
    Width = 185
    Height = 607
    Align = alLeft
    ItemHeight = 13
    PopupMenu = PopupMenu1
    TabOrder = 0
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 735
    Height = 29
    ButtonHeight = 29
    ButtonWidth = 73
    Caption = 'ToolBar1'
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Caption = '123123123'
      ImageIndex = 0
      OnClick = ToolButton1Click
    end
  end
  object Button2: TButton
    Left = 270
    Top = 541
    Width = 75
    Height = 25
    Caption = 'Delete link'
    TabOrder = 2
    OnClick = Button2Click
  end
  object ListBox2: TListBox
    Left = 432
    Top = 29
    Width = 303
    Height = 607
    Align = alRight
    ItemHeight = 13
    TabOrder = 3
    OnDblClick = ListBox2DblClick
  end
  object Button1: TButton
    Left = 191
    Top = 541
    Width = 75
    Height = 25
    Caption = '<-'
    Enabled = False
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button3: TButton
    Left = 351
    Top = 541
    Width = 75
    Height = 25
    Caption = '->'
    Enabled = False
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 191
    Top = 572
    Width = 235
    Height = 25
    Caption = 'delete component'
    TabOrder = 6
    OnClick = Button4Click
  end
  object PopupMenu1: TPopupMenu
    Left = 384
    Top = 32
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1074#1103#1079#1100
      OnClick = N1Click
    end
  end
end
