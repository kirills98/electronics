unit Event;

interface

uses Windows, Classes, SysUtils, Dialogs;

type
  TTypeEvent = (evMove, evCreate, evDelete);
  TTypeData = (dtComponent, dtConnection);


  TData = record
    typeData: TTypeData;
    componentName: string;
    connectionComponentName1, connectionComponentName2:string;
    coord: TPoint;
  end;

  PEvent = ^TEvent;
  TEvent = class(TObject)
    id:integer;
    timestamp: TTimeStamp;
    typeEvent: TTypeEvent;
    data: TData;
    constructor Create(list:TList; typeEvent:TTypeEvent; typeData: TTypeData; componentName, connectionComponent1Name, connectionComponent2Name :string; x, y:integer);
    constructor CreateComponent(list:TList; componentName:string; x, y:integer);
    constructor CreateConnection(list:TList; component1Name, component2Name:string);
    constructor MoveComponent(list:TList; componentName:string; x, y:integer);
    constructor DeleteComponent(list:TList; componentName:string);
    constructor DeleteConnection(list:TList; component1Name, component2Name:string);
    procedure saveToStream;
  end;

implementation

constructor TEvent.Create(
  list:TList;
  typeEvent:TTypeEvent;
  typeData: TTypeData;
  componentName, connectionComponent1Name, connectionComponent2Name :string;
  x, y:integer
);
begin
  self.id := list.Count;
  self.timestamp := DateTimeToTimeStamp(Now);
  self.typeEvent := typeEvent;
  self.data.typeData := typeData;
  self.data.componentName := componentName;
  self.data.connectionComponentName1 := connectionComponent1Name;
  self.data.connectionComponentName2 := connectionComponent2Name;
  self.data.coord.X := x;
  self.data.coord.Y := Y;
  list.Add(self);
//  saveToStream;
end;

constructor TEvent.CreateComponent(list:TList; componentName:string; x, y:integer);
begin
  Create(
    list,
    evCreate,
    dtComponent,
    componentName,
    '','',
    x,y
  );
end;

constructor TEvent.CreateConnection(list:TList; component1Name, component2Name:string);
begin
  create(
    list,
    evCreate,
    dtConnection,
    '',
    component1Name,
    component2Name,
    -1,-1
  );
end;

constructor TEvent.MoveComponent(list:TList; componentName:string; x, y:integer);
begin
  create(
    list,
    evMove,
    dtComponent,
    componentName,
    '','',
    x,y
  );
end;

constructor TEvent.DeleteComponent(list:TList; componentName:string);
begin
  create(
    list,
    evDelete,
    dtComponent,
    componentName,
    '', '',
    -1,-1
  );
end;

constructor TEvent.DeleteConnection(list:TList; component1Name, component2Name:string);
begin
  create(
    list,
    evDelete,
    dtComponent,
    '',
    component1Name,
    component2Name,
    -1, -1
  );
end;

procedure TEvent.saveToStream;
var
  stream:TMemoryStream;
  e:TEvent;
  s : string;
begin
  stream := TMemoryStream.Create;
  stream.Write(self, self.InstanceSize);
  stream.SaveToFile('test.str');
  stream.Read(TEvent(e), stream.Size);
  showmessage(e.data.componentName);
end;

end.
