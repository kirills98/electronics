object fmSystemDlg: TfmSystemDlg
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = #1042#1093#1086#1076
  ClientHeight = 179
  ClientWidth = 384
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 25
    Top = 75
    Width = 33
    Height = 13
    Caption = #1050#1083#1072#1089#1089':'
  end
  object Label2: TLabel
    Left = 31
    Top = 102
    Width = 27
    Height = 13
    Caption = #1060#1048#1054':'
  end
  object Label3: TLabel
    Left = 17
    Top = 129
    Width = 41
    Height = 13
    Caption = #1055#1072#1088#1086#1083#1100':'
  end
  object Shape1: TShape
    Left = 272
    Top = 8
    Width = 1
    Height = 153
  end
  object OKBtn: TButton
    Left = 300
    Top = 8
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 300
    Top = 38
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object RadioGroup1: TRadioGroup
    Left = 8
    Top = 8
    Width = 241
    Height = 41
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      #1059#1095#1077#1085#1080#1082
      #1059#1095#1080#1090#1077#1083#1100)
    TabOrder = 2
    OnClick = RadioGroup1Click
  end
  object ComboBox1: TComboBox
    Left = 64
    Top = 72
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    OnChange = RadioGroup1Click
  end
  object ComboBox2: TComboBox
    Left = 64
    Top = 99
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 4
  end
  object Edit1: TEdit
    Left = 64
    Top = 126
    Width = 145
    Height = 21
    PasswordChar = '*'
    TabOrder = 5
  end
end
