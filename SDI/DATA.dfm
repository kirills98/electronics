object dmData: TdmData
  OldCreateOrder = False
  Height = 222
  Width = 295
  object quClasses: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    SQL.Strings = (
      'SELECT  *  FROM CLASSES_')
    Left = 56
    Top = 88
  end
  object IBDatabase1: TIBDatabase
    Connected = True
    DatabaseName = 'C:\Users\14001\Documents\ELECTRONICKS.FDB'
    Params.Strings = (
      'user_name=SYSDBA'
      'password=masterkey')
    LoginPrompt = False
    DefaultTransaction = IBTransaction1
    Left = 32
    Top = 32
  end
  object IBTransaction1: TIBTransaction
    Active = True
    DefaultDatabase = IBDatabase1
    Left = 128
    Top = 32
  end
  object quUsers: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    SQL.Strings = (
      'select * from users where classname = :CLASSNAME;')
    Left = 104
    Top = 88
    ParamData = <
      item
        DataType = ftString
        Name = 'CLASSNAME'
        ParamType = ptInput
      end>
  end
  object spPassword: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'CHECKPASSWORD'
    Left = 184
    Top = 32
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CNT'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'PASSWORD_'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_USER'
        ParamType = ptInput
      end>
  end
  object quLabs: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    SQL.Strings = (
      'select * from LABS')
    Left = 56
    Top = 152
  end
  object spOperation: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'INSOPERATION'
    Left = 248
    Top = 32
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_LAB'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_USER'
        ParamType = ptInput
      end>
  end
end
