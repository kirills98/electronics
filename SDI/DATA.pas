unit DATA;

interface

uses
  SysUtils, Classes, IBDatabase, DB, IBCustomDataSet, IBQuery, IBStoredProc;

type
  TdmData = class(TDataModule)
    quClasses: TIBQuery;
    IBDatabase1: TIBDatabase;
    IBTransaction1: TIBTransaction;
    quUsers: TIBQuery;
    spPassword: TIBStoredProc;
    quLabs: TIBQuery;
    spOperation: TIBStoredProc;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmData: TdmData;

implementation

{$R *.dfm}

end.
