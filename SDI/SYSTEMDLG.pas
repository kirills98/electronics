unit SYSTEMDLG;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TfmSystemDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    RadioGroup1: TRadioGroup;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Shape1: TShape;
    procedure RadioGroup1Click(Sender: TObject);
  private
    procedure setUsers(CName: String);
  public
      procedure setClass;
  end;

var
  fmSystemDlg: TfmSystemDlg;

implementation

uses Data,SDIMAIN;

{$R *.dfm}

procedure TfmSystemDlg.setUsers(CName: String);
var
  id: integer;
begin
  ComboBox2.Clear;
  with dmData.quUsers do begin
    Close;
    parambyname('ClassName').AsString:= CName;
    Open;
    while not eof do begin
      id:=fieldbyname('ID_USER').AsInteger;
      ComboBox2.Items.AddObject(FieldbyName('UserName_').AsString, TObject(id));
      next;
    end;
  end;
end;

procedure TfmSystemDlg.RadioGroup1Click(Sender: TObject);
begin
  if RadioGroup1.ItemIndex = 1  then setusers('Teacher') else setusers(ComboBox1.text);
end;

procedure TfmSystemDlg.setClass;
begin
   ComboBox1.Clear;
  with dmData.quClasses do begin
    Close;
    Open;
    while not eof do begin
      ComboBox1.Items.Add(FieldbyName('ClassName').AsString);
      next;
    end;
  end;
end;


end.
