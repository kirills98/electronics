program dclElectronics;

uses
  Forms,
  Main in '..\MDI\Main.pas' {MainForm},
  ChildWin in '..\MDI\ChildWin.pas' {MDIChild},
  about in '..\MDI\about.pas' {AboutBox},
  Data in '..\MDI\Data.pas' {dmData: TDataModule},
  Event in '..\MDI\Event.pas',
  Unit1 in '..\MDI\Unit1.pas' {Form1};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TdmData, dmData);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
