unit LogElements;

interface

uses
  SysUtils, Classes, Controls, Graphics, Windows ;

const
  _Size = 28;
  sMax  =27;
type
  TDirection = (dVertical , dHorisontal);
  TDirectionNode = (dnUp , dnDown, dnLeft , dnRight);

  TLogElement = class(TGraphicControl)
  private
    { Private declarations }
    fDirection : TDirection;
  protected
    { Protected declarations }
    BitMapH , BitMapV: Graphics.TBitMap;
  public
    { Public declarations }
    selected: boolean;
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
    procedure Turn;
  published
    { Published declarations }
    property OnMouseUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnClick;
    property OnDblClick;
    property DragMode;
    property PopupMenu;
    property DragCursor;
    property DragKind;
    property ShowHint;
    property Direction : TDirection read fDirection write fDirection;
  end;

  TLamp = class(TLogElement)
  public
      private
      flight:real;
      fumax:real;
      fimax:real;
      protected
        procedure SetOn (Value : real );
    public
      constructor Create(Owner: TComponent);
      procedure Paint; override;
    published
      property light:real Read flight Write SetOn;
      property umax :real Read fumax Write fumax;
      property imax :real Read fimax Write fimax;
  end;


  TKey = class(TLogElement)
    private
      fActive:boolean;
      protected
        procedure SetOn (Value : boolean );
    public
      constructor Create(Owner: TComponent);
      procedure Paint; override;
    published
      property  Active:boolean Read fActive Write SetOn;
  end;

  TResistor = class(TLogElement)
    private
      fValue:real;
    protected
      procedure SetOn (Value : real );
    published
      property  Value:real Read fValue Write SetOn;
  end;

  TAmpermeter = class(TLogElement)
  private
    fumax:real;
    fimax:real;
    fValue:real;
  published
    property Value :real Read fValue Write fvalue;
    property umax :real Read fumax Write fumax;
    property imax :real Read fimax Write fimax;
   end;

  TVoltmeter = class(TLogElement)
  private
    fValue:real;
  published
    property Value :real Read fValue Write fvalue;
   end;

  TBattery = class(TLogElement)
    private
      fvalue:real;
    published
      property  value:real Read fvalue Write fvalue;
 end;

  TNode = class(TGraphicControl)
  private
    { Private declarations }
    fDirectionNode : TDirectionNode;
  protected
    { Protected declarations }
    BitMapUp , BitMapDown ,BitMapLeft ,BitMapRight : Graphics.TBitMap;
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
    procedure Turn;
    published
    { Published declarations }
    property OnClick;
    property OnDblClick;
    property DragMode;
    property DragCursor;
    property DragKind;
    property ShowHint;
    property DirectionNode : TDirectionNode read fDirectionNode write fDirectionNode;
  end;

procedure Register;

implementation

{$R *.res}

{TNode (���� ) �������  }

constructor TNode.Create(Owner: TComponent);
var
  s: string;
  ResName: array[0..40] of Char;
begin
  inherited Create(Owner);
  Width:=_Size;
  Height:=_Size;
  BitMapUp := Graphics.TBitmap.Create;
  BitMapDown := Graphics.TBitmap.Create;
  BitMapRight := Graphics.TBitmap.Create;
  BitMapLeft := Graphics.TBitmap.Create;
  s:= className + 'Up';
  StrPCopy(ResName, s);
  BitMapUp.Handle := LoadBitmap(HInstance, ResName);
  BitMapUp.Transparent:=true;
  s:= className + 'Down';
  StrPCopy(ResName, s);
  BitMapDown.Handle := LoadBitmap(HInstance, ResName);
  BitMapDown.Transparent:=true;
    s:= className + 'Right';
  StrPCopy(ResName, s);
  BitMapRight.Handle := LoadBitmap(HInstance, ResName);
  BitMapRight.Transparent:=true;
    s:= className + 'Left';
  StrPCopy(ResName, s);
  BitMapLeft.Handle := LoadBitmap(HInstance, ResName);
  BitMapLeft.Transparent:=true;
  Constraints.MaxHeight:=_Size;
  Constraints.MinHeight:=_Size;
  Constraints.MaxWidth:=_Size;
  Constraints.MinWidth:=_Size;
end;

destructor TNode.Destroy;
begin
  BitMapUp.Destroy;
  BitMapDown.Destroy;
  BitMapRight.Destroy;
  BitMapLeft.Destroy;
  inherited Destroy;
end;

procedure TNode.Paint;
begin
  if csDesigning in ComponentState then
  begin
    canvas.Pen.Width:=1; 
    Canvas.Pen.Color:=clRed;
    Canvas.Brush.Color:=clBtnFace;
    Canvas.Rectangle(0, 0, Width, Height);
  end;
  case fDirectionNode of
  dnUp : Canvas.Draw(0, 0, BitMapUp);
  dnDown   : Canvas.Draw(0, 0, BitMapDown);
  dnRight :  Canvas.Draw(0, 0, BitMapRight);
  dnLeft :  Canvas.Draw(0, 0, BitMapLeft);
  end;

end;

procedure TNode.Turn;
begin
  if fDirectionNode = dnUp then fDirectionNode :=  dnRight
  else if fDirectionNode = dnRight then fDirectionNode :=  dnDown
  else if fDirectionNode = dnDown then fDirectionNode :=  dnLeft
  else fDirectionNode := dnUp;
end;


 {��� ������� �� ���� ���������� ��� ��������� �������� ------����� ��������  -----------------------}

 constructor TLogElement.Create(Owner: TComponent);
var
  s: string;
  ResName: array[0..40] of Char;
begin
  inherited Create(Owner);
  Width:=_Size;
  Height:=_Size;
  BitMapH := Graphics.TBitmap.Create;
  BitMapV := Graphics.TBitmap.Create;
  s:= className + 'H';
  StrPCopy(ResName, s);
  BitMapH.Handle := LoadBitmap(HInstance, ResName);
  BitMapH.Transparent:=true;
  s:= className + 'V';
  StrPCopy(ResName, s);
  BitMapV.Handle := LoadBitmap(HInstance, ResName);
  BitMapV.Transparent:=true;
  Constraints.MaxHeight:=_Size;
  Constraints.MinHeight:=_Size;
  Constraints.MaxWidth:=_Size;
  Constraints.MinWidth:=_Size;
end;

destructor TLogElement.Destroy;
begin
  BitMapH.Destroy;
  BitMapV.Destroy;
  inherited Destroy;
end;

procedure TLogElement.Paint;
begin
  if (csDesigning in ComponentState) or selected then
  begin
    canvas.Pen.Width:=1; // modific 9:12 25.01
    Canvas.Pen.Color:=clRed;
    Canvas.Brush.Color:=clBtnFace;
    Canvas.Rectangle(0, 0, Width, Height);
  end;
  case fDirection of
  dHorisontal : Canvas.Draw(0, 0, BitMapH);
  dVertical   : Canvas.Draw(0, 0, BitMapV);
  end;

end;

procedure TLogElement.Turn;
begin
  if fDirection = dHorisontal then fDirection :=  dVertical
  else fDirection := dHorisontal;
end;

{�����------------------------------------}
procedure TLamp.SetOn (Value : real );
begin
  flight := value ;
  fumax := value ;
  paint;

end;

constructor TLamp.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  flight := 0.0;
  fumax:= 60.0;
end;

procedure TLamp.Paint;
begin
  inherited Paint;
  canvas.Pen.Width:=1;
case fDirection of
  dHorisontal :
  begin
  if (flight > 0.0) and (flight <= umax*1/3) then
  begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(2,2,sMax-2,sMax-2 , sMax,0,0,0);  {del 2-s ; 3-s arc  }
    canvas.Arc(2,2,sMax-2,sMax-2 , 0,sMax,sMax,sMax);
    canvas.Arc(1,1,sMax-1,sMax-1 , sMax,0,0,0);
    canvas.Arc(1,1,sMax-1,sMax-1 , 0,sMax,sMax,sMax);
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,sMax-3,sMax-3 , sMax,0,0,0);       {1-s arc }
    canvas.Arc(3,3,sMax-3,sMax-3 , 0,sMax,sMax,sMax);
  end
  else if (flight > umax*1/3) and (flight <= umax*2/3)then
  begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(1,1,sMax-1,sMax-1 , sMax,0,0,0);  {del 3-s arc  }
    canvas.Arc(1,1,sMax-1 , sMax-1 , 0,sMax,sMax,sMax);
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,sMax-3,sMax-3,sMax,0,0,0);       {1-s arc }
    canvas.Arc(3,3,sMax-3,sMax-3,0,sMax,sMax,sMax);
    canvas.Arc(2,2,sMax-2,sMax-2,sMax,0,0,0);      {2-s arc }
    canvas.Arc(2,2,sMax-2,sMax-2,0,sMax,sMax,sMax);
  end
  else if (flight > umax*2/3) and (flight <= umax) then
  begin
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,sMax-3,sMax-3 , sMax,0,0,0);       {1-s arc }
    canvas.Arc(3,3,sMax-3,sMax-3 , 0,sMax,sMax,sMax);
    canvas.Arc(2,2,sMax-2,sMax-2 , sMax,0,0,0);      {2-s arc }
    canvas.Arc(2,2,sMax-2,sMax-2 , 0,sMax,sMax,sMax);
    canvas.Arc(1,1,sMax-1,sMax-1 , sMax,0,0,0);      {3-s arc }
    canvas.Arc(1,1,sMax-1,sMax-1 , 0,sMax,sMax,sMax);
  end
  else
   begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(3,3,sMax-3,sMax-3 , sMax,0,0,0);       {del all arc  }
    canvas.Arc(3,3,sMax-3,sMax-3 , 0,sMax,sMax,sMax);
    canvas.Arc(2,2,sMax-2,sMax-2 , sMax,0,0,0);
    canvas.Arc(2,2,sMax-2,sMax-2 , 0,sMax,sMax,sMax);
    canvas.Arc(1,1,sMax-1,sMax-1 , sMax,0,0,0);
    canvas.Arc(1,1,sMax-1,sMax-1 , 0,sMax,sMax,sMax);
   end;
  end;
dVertical:
 begin
  if (flight > 0.0) and (flight <= umax*1/3) then
  begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(2,2,sMax-2,sMax-2 , 0,0,0,sMax);  // del 2-s ; 3-s arc
    canvas.Arc(2,2,sMax-2,sMax-2 , sMax,sMax,sMax,0);
    canvas.Arc(1,1,sMax-1,sMax-1 , 0,0,0,sMax);
    canvas.Arc(1,1,sMax-1,sMax-1 , sMax,sMax,sMax,0);
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,sMax-3,sMax-3 , 0,0,0,sMax);           //1-s arc
    canvas.Arc(3,3,sMax-3,sMax-3 , sMax,sMax,sMax,0);
  end
  else if (flight > umax*1/3) and (flight <= umax*2/3)then
  begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(1,1,sMax-1,sMax-1 , 0,0,0,sMax);  //del 3-s arc
    canvas.Arc(1,1,sMax-1 , sMax-1 , sMax,sMax,sMax,0);
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,sMax-3,sMax-3 , 0,0,0,sMax);       //1-s arc
    canvas.Arc(3,3,sMax-3,sMax-3 , sMax,sMax,sMax,0);
    canvas.Arc(2,2,sMax-2,sMax-2 , 0,0,0,sMax);      //2-s arc
    canvas.Arc(2,2,sMax-2,sMax-2 , sMax,sMax,sMax,0);
  end
  else if (flight > umax*2/3) and (flight <= umax) then
  begin
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,sMax-3,sMax-3 , 0,0,0,sMax);      //1-s arc
    canvas.Arc(3,3,sMax-3,sMax-3 , sMax,sMax,sMax,0);
    canvas.Arc(2,2,sMax-2,sMax-2 , 0,0,0,sMax);      //2-s arc
    canvas.Arc(2,2,sMax-2,sMax-2 , sMax,sMax,sMax,0);
    canvas.Arc(1,1,sMax-1,sMax-1 , 0,0,0,sMax);    //  3-s arc
    canvas.Arc(1,1,sMax-1,sMax-1 , sMax,sMax,sMax,0);
  end
  else
   begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(3,3,sMax-3,sMax-3 , 0,0,0,sMax);     //  del all arc
    canvas.Arc(3,3,sMax-3,sMax-3 , sMax,sMax,sMax,0);
    canvas.Arc(2,2,sMax-2,sMax-2 , 0,0,0,sMax);
    canvas.Arc(2,2,sMax-2,sMax-2 , sMax,sMax,sMax,0);
    canvas.Arc(1,1,sMax-1,sMax-1 , 0,0,0,sMax);
    canvas.Arc(1,1,sMax-1,sMax-1 , sMax,sMax,sMax,0);
   end;
 end;
end;

end;



{����------------------------------------- }
procedure TKey.SetOn (Value : boolean );
begin
  fActive := value ;
  paint;

end;

constructor TKey.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  fActive:= false ;
end;

procedure TKey.Paint;
begin
  inherited Paint;
  canvas.MoveTo(19,14);        {��� ����� }
  canvas.Pen.Width:=2;
case fDirection of
  dHorisontal:
  begin
    if fActive then
      begin
       Canvas.Pen.Color:=clBtnFace;
       canvas.LineTo(14,6)  ;   {�������. ����� �� ��� ����� � ������� ��������� �����}
       canvas.MoveTo(19,14);
       Canvas.Pen.Color:=clBlack;
       canvas.LineTo(9,13);     {����� �� ��� �����. � ����������  }
      end
      else
          begin
            Canvas.Pen.Color:=clBtnFace;
            canvas.LineTo(9,13);        {������� . ����� �� ��� �����. � ����������  }
            canvas.MoveTo(19,14);
            Canvas.Pen.Color:=clBlack;
            canvas.LineTo(14,6);
          end;
    end;
  dVertical:                    {��������� ��������� ����� � ������������ ����}
  begin
       canvas.MoveTo(14,8);         {��� �}
        if fActive then
      begin
       Canvas.Pen.Color:=clBtnFace;
       canvas.LineTo(6,13)  ;          {����. ����� �� �������� ����� . }
       Canvas.Pen.Color:=clBlack;
       canvas.MoveTo(14,8);
       canvas.LineTo(13,18);          {��� ��� �������� ���� }
      end
      else
          begin
            canvas.MoveTo(14,8);
            Canvas.Pen.Color:=clBtnFace;
            canvas.LineTo(13,18);
            Canvas.Pen.Color:=clBlack;
            canvas.MoveTo(14,8);
            canvas.LineTo(6,13);
          end;
  end;
end;
  canvas.Pen.Width:=1;
end;

{��������------------------------------------- }

procedure TResistor.SetOn (Value : real );
begin
    fValue := value ;
end;




procedure Register;
begin
  RegisterComponents('Electronics', [TLogElement, TLamp, TKey ,TResistor , TAmpermeter , TVoltmeter, TBattery ,TNode]);
end;

end.
