unit Lamps;

interface

uses
  SysUtils, Classes, Controls, Graphics, Windows, LogElements;

const
  _Size = 28;

type
  TLamp = class(TLogElement)
  private
    { Private declarations }
    BitMap: Graphics.TBitMap;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
  published
    { Published declarations }
  end;

implementation

constructor TLamp.Create(Owner: TComponent);
var
  ResName: array[0..40] of Char;
begin
  inherited Create(Owner);
  Width:=_Size;
  Height:=_Size;
  BitMap := Graphics.TBitmap.Create;
  StrPCopy(ResName, 'TLamp');
  BitMap.Handle := LoadBitmap(HInstance, ResName);
  BitMap.Transparent:=true;
  Constraints.MaxHeight:=_Size;
  Constraints.MinHeight:=_Size;
  Constraints.MaxWidth:=_Size;
  Constraints.MinWidth:=_Size;
end;

destructor TLamp.Destroy;
begin
  BitMap.Destroy;       
  inherited Destroy;
end;

procedure TLamp.Paint;
begin
  if csDesigning in ComponentState then
  begin
    Canvas.Pen.Color:=clRed;
    Canvas.Brush.Color:=clBtnFace;
    Canvas.Rectangle(0, 0, Width, Height);
  end;
  Canvas.Draw(0, 0, BitMap);
end;

end.
