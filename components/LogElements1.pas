unit LogElements1;

interface

uses
  SysUtils, Classes, Controls;

type
  TLogElement = class(TGraphicControl)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Samples', [TLogElement]);
end;

end.
