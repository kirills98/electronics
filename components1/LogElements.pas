unit LogElements;

interface

uses
  SysUtils, Classes, Controls, Graphics, Windows ;

const
  _Size = 28;

type
  TDirection = (dVertical, dHorisontal);

  TLogElement = class(TGraphicControl)
  private
    { Private declarations }
    fDirection : TDirection;
  protected
    { Protected declarations }
    BitMap: Graphics.TBitMap;
  public
    { Public declarations }
    selected: boolean;
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
  published
    { Published declarations }
    property OnMouseUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnClick;
    property OnDblClick;
    property DragMode;
    property DragCursor;
    property DragKind;
    property ShowHint;
    property Direction : TDirection read fDirection write fDirection;
  end;

  TLamp = class(TLogElement)
  public
      private
      flight:real;
      fumax:real;
      fimax:real;
      protected
        procedure SetOn (Value : real );
    public
      constructor Create(Owner: TComponent);
      procedure Paint; override;
    published
      property light:real Read flight Write SetOn;
      property umax :real Read fumax Write fumax;
      property imax :real Read fimax Write fimax;
  end;


  TKey = class(TLogElement)
    private
      fActive:boolean;
      protected
        procedure SetOn (Value : boolean );
    public
      constructor Create(Owner: TComponent);
      procedure Paint; override;
    published
      property  Active:boolean Read fActive Write SetOn;
  end;

  TRez = class(TLogElement)
    private
      fValue:real;
    protected
      procedure SetOn (Value : real );
    published
      property  Value:real Read fValue Write SetOn;
  end;

  TAmp = class(TLogElement)
  private
    fumax:real;
    fimax:real;
    fValue:real;
  published
    property Value :real Read fValue Write fvalue;
    property umax :real Read fumax Write fumax;
    property imax :real Read fimax Write fimax;
   end;

  TVol = class(TLogElement)
  private
    fValue:real;
  published
    property Value :real Read fValue Write fvalue;
   end;

  TPow = class(TLogElement)
    private
    fvalue:real;
    published
      property  value:real Read fvalue Write fvalue;
 end;


procedure Register;

implementation

{$R *.res}

constructor TLogElement.Create(Owner: TComponent);
var
  ResName: array[0..40] of Char;
begin
  inherited Create(Owner);
  Width:=_Size;
  Height:=_Size;
  BitMap := Graphics.TBitmap.Create;
  StrPCopy(ResName, className);
  BitMap.Handle := LoadBitmap(HInstance, ResName);
  BitMap.Transparent:=true;
  Constraints.MaxHeight:=_Size;
  Constraints.MinHeight:=_Size;
  Constraints.MaxWidth:=_Size;
  Constraints.MinWidth:=_Size;
end;

destructor TLogElement.Destroy;
begin
  BitMap.Destroy;
  inherited Destroy;
end;

procedure TLogElement.Paint;
begin
  if (csDesigning in ComponentState) or selected then
  begin
    Canvas.Pen.Color:=clRed;
    Canvas.Brush.Color:=clBtnFace;
    Canvas.Rectangle(0, 0, Width, Height);
  end;
  Canvas.Draw(0, 0, BitMap);
end;


{�����------------------------------------}
procedure TLamp.SetOn (Value : real );
begin
  flight := value ;
  paint;

end;

constructor TLamp.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  flight:= 0.0 ;
  fumax:= 60.0;
end;

procedure TLamp.Paint;

begin
  inherited Paint;
  canvas.Pen.Width:=1;
//  if fDirection = dHorisontal then
//  begin
  if (flight > 0.0) and (flight <= umax*1/3) then
  begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(2,2,25,25,27,0,0,0);  {del 2-s ; 3-s arc  }
    canvas.Arc(2,2,25,25,0,27,27,27);
    canvas.Arc(1,1,26,26,27,0,0,0);
    canvas.Arc(1,1,26,26,0,27,27,27);
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,24,24,27,0,0,0);       {1-s arc }
    canvas.Arc(3,3,24,24,0,27,27,27);
  end
  else if (flight > umax*1/3) and (flight <= umax*2/3)then
  begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(1,1,26,26,27,0,0,0);  {del 3-s arc  }
    canvas.Arc(1,1,26,26,0,27,27,27);
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,24,24,27,0,0,0);       {1-s arc }
    canvas.Arc(3,3,24,24,0,27,27,27);
    canvas.Arc(2,2,25,25,27,0,0,0);      {2-s arc }
    canvas.Arc(2,2,25,25,0,27,27,27);
  end
  else if (flight > umax*2/3) and (flight <= umax) then
  begin
    Canvas.Pen.Color:=clYellow;
    canvas.Arc(3,3,24,24,27,0,0,0);       {1-s arc }
    canvas.Arc(3,3,24,24,0,27,27,27);
    canvas.Arc(2,2,25,25,27,0,0,0);      {2-s arc }
    canvas.Arc(2,2,25,25,0,27,27,27);
    canvas.Arc(1,1,26,26,27,0,0,0);      {3-s arc }
    canvas.Arc(1,1,26,26,0,27,27,27);
  end
  else
   begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.Arc(3,3,24,24,27,0,0,0);       {del all arc  }
    canvas.Arc(3,3,24,24,0,27,27,27);
    canvas.Arc(2,2,25,25,27,0,0,0);
    canvas.Arc(2,2,25,25,0,27,27,27);
    canvas.Arc(1,1,26,26,27,0,0,0);
    canvas.Arc(1,1,26,26,0,27,27,27);
   end;
//  end;

end;



{����------------------------------------- }
procedure TKey.SetOn (Value : boolean );
begin
  fActive := value ;
  paint;

end;

constructor TKey.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  fActive:= false ;
end;

procedure TKey.Paint;
begin
  inherited Paint;
  canvas.MoveTo(19,14);
  canvas.Pen.Width:=2;
  if fActive then
  begin
    Canvas.Pen.Color:=clBtnFace;
    canvas.LineTo(14,6)  ;
    canvas.MoveTo(19,14);
    Canvas.Pen.Color:=clBlack;
    canvas.LineTo(9,13);
  end
  else
  begin
     Canvas.Pen.Color:=clBtnFace;
     canvas.LineTo(9,13);
     canvas.MoveTo(19,14);
    Canvas.Pen.Color:=clBlack;
    canvas.LineTo(14,6);
  end;
end;

{��������------------------------------------- }

procedure TRez.SetOn (Value : real );
begin
    fValue := value ;
end;




procedure Register;
begin
  RegisterComponents('Electronics', [TLogElement, TLamp, TKey ,TRez , TAmp , TVol, TPow]);
end;

end.
