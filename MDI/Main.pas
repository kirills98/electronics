unit MAIN;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, Menus,
  StdCtrls, Dialogs, Buttons, Messages, ExtCtrls, ComCtrls, StdActns,
  ActnList, ToolWin, ImgList, Event, CHILDWIN, DB, LogElements;

type
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    FileNewItem: TMenuItem;
    FileOpenItem: TMenuItem;
    FileCloseItem: TMenuItem;
    Window1: TMenuItem;
    Help1: TMenuItem;
    N1: TMenuItem;
    FileExitItem: TMenuItem;
    WindowCascadeItem: TMenuItem;
    WindowTileItem: TMenuItem;
    WindowArrangeItem: TMenuItem;
    HelpAboutItem: TMenuItem;
    OpenDialog: TOpenDialog;
    FileSaveItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    Edit1: TMenuItem;
    CutItem: TMenuItem;
    CopyItem: TMenuItem;
    PasteItem: TMenuItem;
    WindowMinimizeItem: TMenuItem;
    StatusBar: TStatusBar;
    ActionList1: TActionList;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    FileNew1: TAction;
    FileSave1: TAction;
    FileExit1: TAction;
    FileOpen1: TAction;
    FileSaveAs1: TAction;
    WindowCascade1: TWindowCascade;
    WindowTileHorizontal1: TWindowTileHorizontal;
    WindowArrangeAll1: TWindowArrange;
    WindowMinimizeAll1: TWindowMinimizeAll;
    HelpAbout1: TAction;
    FileClose1: TWindowClose;
    WindowTileVertical1: TWindowTileVertical;
    WindowTileItem2: TMenuItem;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton9: TToolButton;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ImageList1: TImageList;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton7: TToolButton;
    N2: TMenuItem;
    Undo1: TMenuItem;
    Redo1: TMenuItem;
    ToolButton3: TToolButton;
    Delete1: TMenuItem;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    procedure CreateLamp(Sender: TObject);
    procedure CreateAmp(Sender: TObject);
    procedure HelpAbout1Execute(Sender: TObject);
    procedure FileExit1Execute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CreateKey(Sender: TObject);
    procedure CreatePow(Sender: TObject);
    procedure CreateRez(Sender: TObject);
    procedure CreateVol(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    ID_Lab, ID_User, sdi_right, sdi_top: integer;
    { Private declarations }
    procedure CreateMDIChild(labName, userName: string);
    procedure saveEventsToBD;
    procedure loadEventsFromBD;
  public
    { Public declarations }
    Child: TMDIChild;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses  about, Data;

procedure TMainForm.loadEventsFromBD;
var
  list:TList;
  stream:TStream;
  s:string;
  n, i, len:integer;
  e:Tevent;
begin
  with dmData.quSelectEvents do
  begin
    close;
    paramByName('id_lab').AsInteger := ID_Lab;
    paramByName('id_user').AsInteger := ID_User;
    open;
    stream := CreateBlobStream(fieldByName('DATAEVENT'), bmRead);
  end;
//  list := Tlist.Create;
  Child.eventlist.clear;
  stream.Position := 0;
  stream.Read(n, sizeOf(n));
  for i := 0 to n - 1 do
  begin
    e := TEvent.Create;
    stream.Read(e.id, sizeOf(e.id));
//    showmessage(inttostr(e.id));
    stream.Read(e.timestamp, sizeOf(e.timestamp));
    stream.Read(e.typeEvent, sizeOf(e.typeEvent));
    stream.Read(e.data.typeData, sizeOf(e.data.typeData));
    stream.Read(e.data.componenType, sizeOf(e.data.componenType));
    stream.Read(e.data.n, sizeOf(e.data.n));
    stream.Read(len, sizeOf(len));
    SetLength(s, len);
    stream.Read(s[1], len);
    e.data.connectionComponentName1 := s;
    stream.Read(len, sizeOf(len));
    SetLength(s, len);
    stream.Read(s[1], len);
    e.data.connectionComponentName2 := s;
    stream.Read(e.data.coord.X, sizeOf(e.data.coord.X));
    stream.Read(e.data.coord.Y, sizeOf(e.data.coord.Y));
//    showmessage(e.toString);
    Child.eventList.Add(e);
  end;
  stream.free;
  Child.step := Child.eventList.Count - 1;
  Child.toEvent(Child.step);
  for I := 0 to child.ComponentCount - 1 do
  begin
    if (child.Components[i] is TLogElement) then
    begin
      inc(child.cnt);
    end;    
  end;

  Child.Invalidate;
end;

procedure TMainForm.saveEventsToBD;
var 
  stream:TMemoryStream;
  buffer: Pointer;
  e:TEvent;
  s : string;
  i, n, len:integer;
begin
  stream := TMemoryStream.Create;
  n := Child.eventList.Count;
  stream.WriteBuffer(n, sizeOf(n));
  for I := 0 to n - 1 do
  begin
    e := Child.eventList[i];
    stream.writeBuffer(e.id, sizeOf(e.id));
    stream.writeBuffer(e.timestamp, sizeOf(e.timestamp));
    stream.writeBuffer(e.typeEvent, sizeOf(e.typeEvent));
    stream.writeBuffer(e.data.typeData, sizeOf(e.data.typeData));
    stream.writeBuffer(e.data.componenType, sizeOf(e.data.componenType));
    stream.writeBuffer(e.data.n, sizeOf(e.data.n));
    s := e.data.connectionComponentName1;
    len := length(s);
    stream.writeBuffer(len, sizeOf(len));
    stream.writeBuffer(s[1], len);
    s := e.data.connectionComponentName2;
    len := length(s);
    stream.writeBuffer(len, sizeOf(len));
    stream.writeBuffer(s[1], len);
    len := e.data.coord.X;
    stream.writeBuffer(len, sizeOf(len));
    len := e.data.coord.Y;
    stream.writeBuffer(len, sizeOf(len));
//    stream.Write(Child.eventList, Child.eventList.InstanceSize);
  end;
  stream.Position := 0;  
  GetMem(buffer, stream.Size);
  stream.Read(buffer^, stream.Size);
  with dmData.quUpdateEvents, dmData.IBTransaction1 do
  begin
    if inTransaction then
      commit;
    startTransaction;      
    close;
    paramByName('DATAEVENT').setBlobData(buffer, stream.size);
    paramByName('id_lab').AsInteger := ID_Lab;
    paramByName('id_user').AsInteger := ID_User;
    execSQL;
    commit;
  end;
//  showMessage(inttostr(stream.size));
  freeMem(buffer);
  stream.Free;
end;

procedure TMainForm.CreateMDIChild(labName, userName: string);
begin
  { create a new MDI child window }
  Child := TMDIChild.Create(Application);
  Child.Caption := labName + ' (' + userName + ')';
end;

procedure TMainForm.CreatePow(Sender: TObject);
begin
  Child.addComponent(cmPow);
end;

procedure TMainForm.CreateRez(Sender: TObject);
begin
  Child.addComponent(cmRez);
end;

procedure TMainForm.CreateVol(Sender: TObject);
begin
  Child.addComponent(cmVol);
end;

procedure TMainForm.CreateKey(Sender: TObject);
begin
  Child.addComponent(cmKey);
end;

procedure TMainForm.CreateLamp(Sender: TObject);
begin
//  CreateMDIChild('NONAME' + IntToStr(MDIChildCount + 1));
  Child.addComponent(cmLamp);
end;

procedure TMainForm.CreateAmp(Sender: TObject);
begin
  Child.addComponent(cmAmp);
end;

procedure TMainForm.FormActivate(Sender: TObject);
var
  s, s1:string;
  del, i: integer;
  arr:array[0..3] of integer;
begin
  s:= paramstr(1);
//  s:= '1#3#1#2';
  for I := 0 to 3 do
  begin
    del := pos('#', s);
    if del <> 0 then
      arr[i] := strtoint(copy(s, 1, del-1))
    else
      arr[i] := strtoint(copy(s, 1, length(s)));
    Delete(s, 1, del);
  end;
  
  id_lab :=  arr[0];
  id_user := arr[1];
  sdi_right := arr[2];
  sdi_top := arr[3];

//  with dmData.spOperation, dmData.IBTransaction1 do
//  begin
//    if inTransaction then
//      commit;
//    StartTransaction;
//    close;
//    paramByName('id_lab').AsInteger := id_lab;
//    paramByName('id_user').AsInteger := id_user;
//    execProc;
//    commit;
//  end;


  with dmData.IBQuery1 do
  begin
    close;
    parambyname('ID_Lab').AsInteger:= ID_Lab;
    open;
    s:=fieldbyname('Labname').asstring;
  end;
  with dmData.IBQuery2 do
  begin
    close;
    parambyname('ID_USER').AsInteger:= ID_USER;
    open;
    s1:=fieldbyname('Username_').asstring;
  end;

  CreateMDIChild(s, s1);
  loadEventsFromBD;
  left := sdi_right;
  top := sdi_top;

end;

procedure TMainForm.HelpAbout1Execute(Sender: TObject);
begin
  AboutBox.ShowModal;
end;

procedure TMainForm.ToolButton10Click(Sender: TObject);
begin
  Child.redo;
end;

procedure TMainForm.ToolButton3Click(Sender: TObject);
begin
  Child.deleteSelected;
end;

procedure TMainForm.ToolButton4Click(Sender: TObject);
begin
  saveEventsToBD;
end;

procedure TMainForm.ToolButton5Click(Sender: TObject);
begin
 loadEventsFromBD;
end;

procedure TMainForm.ToolButton8Click(Sender: TObject);
begin
  Child.undo;
end;

procedure TMainForm.FileExit1Execute(Sender: TObject);
begin
  Close;
end;

end.
