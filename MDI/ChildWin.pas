unit CHILDWIN;

interface

uses Windows, Classes, SysUtils, Graphics, Forms, Controls,
  StdCtrls, LogElements, Event, ExtCtrls, Menus;

type
  TMass = array[0..3] of PPoint;
  PMass = ^TMass;
  TConnectionType = (CtInput, CtOutput);
  PConnection = ^TConnection;
  TConnection = Record
    Name1, Name2:string;
    ConnectionType:TConnectionType;
  End;

  PComponentName = ^TComponentName;
  TComponentName = record
    cmType : TComponentType;
    n:integer;
  end;
  
  TMDIChild = class(TForm)
    compMenu: TPopupMenu;
    Delete2: TMenuItem;
    urn1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LogElement1Click(Sender: TObject);
    procedure LogElement1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure LogElement1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LogElement1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormClick(Sender: TObject);
    procedure Delete2Click(Sender: TObject);
    procedure urn1Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    shiftComp: TLogElement;
    Sent, step, cnt:integer;
    List, eventList:TList;
    child:TMDIChild;
    function calcPoints(listPoints: TList; comp1Points, comp2Points: PMass; n:integer) : Tmass;
    function getCoordComp(comp : TLogElement):PMass;
    function checkPoint(listPoints: TList; p: PPoint): boolean;
    procedure toEvent(id:integer);
//    procedure updateConnectionsList();
    procedure updateEventList;
    procedure clearAfterEventList(n:integer);
    procedure createConnection(comp1Name, comp2Name: string; withOutLog: boolean); overload;
    procedure createConnection(comp1Name, comp2Name: string); overload;
    procedure createComponent(left, top: integer; compType:TComponentType; n:integer; withOutLog:boolean); overload;
    procedure createComponent(left, top: integer; compType:TComponentType;  n:integer);overload;
    procedure moveComponent(left, top: integer; cmType:TComponentType; n:integer; withOutLog:boolean); overload;
    procedure moveComponent(left, top: integer; cmType:TComponentType; n:integer);overload;
    procedure deleteComponent(cmType: TComponentType; n:integer; withOutLog:boolean); overload;
    procedure deleteComponent(cmType: TComponentType; n:integer);overload;
    procedure turnComponent(cmType: TComponentType; n:integer; withOutLog:boolean); overload;
    procedure turnComponent(cmType: TComponentType; n:integer);overload;
    procedure deleteConnection(name1, name2:string; withOutLog:boolean); overload;
    procedure deleteConnection(name1, name2:string);   overload;
    function touchComponent(x, y:integer):boolean;
    procedure addComponent(cmType:TComponentType);
    function getNameFromType(cmType:TComponentType; n:integer):string;
    function getTypeFromName(name:string):PComponentName;
    procedure unselectAllComponents;
    procedure undo;
    procedure redo;
    procedure deleteSelected;
  end;

var
  startX, startY, moveX, moveY: integer;
  moving, shifting, didShift: boolean;
  movingLink: PConnection;

implementation

{$R *.dfm}


procedure TMDIChild.deleteSelected;
var
  log:TLogElement;
  p: PComponentName;
begin
  log := findcomponent(movingLink.Name1) as TLogElement;
  p := getTypeFromName(log.Name);
  deleteComponent(p.cmType, p.n);
  moving := false;
  invalidate;
end;

procedure TMDIChild.updateEventList;
var
  i:integer;
  e:Tevent;
  s1,s2:string;
begin
//  ListBox1.Clear;
//  for i := 0 to eventList.Count - 1 do
//  begin
//    e := eventList[i];
//    if (e.typeEvent = evMove) then
//      s1 := 'move'
//    else if (e.typeEvent = evCreate) then
//      s1 := 'create'
//    else
//      s1 := 'del';
//
//    if (e.data.typeData = dtComponent) then
//      s2 := 'comp'
//    else
//      s2 := 'conn';
//
////    ListBox1.Items.Add(
//      inttostr(e.id) + ' : ' +
//      s1 + ' : ' +
//      datetimetostr(timestamptodatetime(e.timestamp)) + ' : ' +
//      getNameFromType(e.data.componenType, e.data.n) + ' : ' +
//      e.data.connectionComponentName1 + ' : ' +
//      e.data.connectionComponentName2 + ' : ' +
//      s2 + ' : ' +
//      inttostr(e.data.coord.x) + ' : ' +
//      inttostr(e.data.coord.y) + ' : '
////    );
////    if i = step then
////      listbox1.ItemIndex := i;
//  end;
end;

procedure TMDIChild.urn1Click(Sender: TObject);
var p:PComponentName;
begin
  p := getTypeFromName(shiftComp.name);
  turnComponent(p.cmType, p.n);
  invalidate;
end;

procedure TMDIChild.undo;
begin
  if step > -1 then
  begin
    dec(step); 
    toEvent(step);
//    updateEventList;
  end;
end;

procedure TMDIChild.redo;
begin
  if (eventList.Count > 0) and (step < eventList.Count - 1) then
  begin
    inc(step);
    toEvent(step);
//    updateEventList;
  end;
end;

procedure TMDIChild.unselectAllComponents;
var
  i:integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    if (Components[i] is TLogElement) then
      (Components[i] as TLogElement).selected := false;
  end;
    
end;

function TMDIChild.getCoordComp(comp: TLogElement) : PMass;
var
  arr : PMass;
begin
    New(arr);
    New(arr[0]);
    New(arr[1]);
    New(arr[2]);
    New(arr[3]);
    arr[0].X := comp.Left;
    arr[0].Y := comp.Top + comp.Height div 2;
    arr[1].X := comp.Left + comp.Width div 2;
    arr[1].Y := comp.top;
    arr[2].X := comp.Left + comp.Width;
    arr[2].Y := arr[0].Y;
    arr[3].X := arr[1].X;
    arr[3].y := comp.Top + comp.Height;
    result := arr;
end;

function TMDIChild.touchComponent(x, y:integer):boolean;
var
  i:integer;
  comp:TLogElement;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    if (components[i] is TLogElement) then
    begin
      comp := components[i] as TLogElement;
      if
        (comp.Left <= x) and
        (comp.Left + comp.Width >= x) and
        (comp.top <= y) and
        (comp.Top + comp.Height >= y)
      then
      begin
        result := true;
        exit;
      end;
    end;
  end;
  result := false;
end;

function TMDIChild.checkPoint(listPoints: TList; p: PPoint) : boolean;
var
  i:integer;
  point :PPoint;
begin
  for i := 0 to listPoints.Count - 1 do
  begin
    point := PPoint(listPoints.items[i]);
    if ((point.x = p.X) and (point.y = p.y)) then
    begin
      result := false;
      exit;
    end;
  end;
  Result := true;
end;

function TMDIChild.calcPoints(listPoints: TList;
 comp1Points: PMass; comp2Points: PMass; n:integer): TMass;
var
  arr: TMass;
  p1, p2, p3: PPoint;
begin
  New(p1);
  New(arr[0]);
  New(arr[1]);
  New(arr[2]);
  New(p2);
  New(p3);
  if (checkPoint(listPoints, comp1Points[abs(n)])) then
  begin
    p1 := comp1Points[abs(n)];
    if (comp1Points[3].Y < comp2POints[1].Y) then
      p3 := comp2Points[1]
    else if (comp1Points[1].Y > comp2POints[3].Y) then
      p3 := comp2Points[3]
    else
    begin
      p3 := comp2Points[abs(n-2)];
    end;
    p2.X := p3.X;
    p2.Y := p1.Y;
  end
  else
  begin
    p3 := comp2Points[abs(n-2)];
    if (comp1Points[3].Y < comp2POints[1].Y) then
      p1 := comp1Points[3]
    else if (comp1Points[1].Y > comp2POints[3].Y) then
      p1 := comp1Points[1]
    else
    begin
      p1:= comp2Points[abs(n)];
    end;
    p2.X := p1.X;
    p2.Y := p3.Y;
  end;

  arr[0] := p1;
  arr[1] := p2;
  arr[2] := p3;
  result := arr;
end;

function TMDIChild.getTypeFromName(name:string) :PComponentName;
var
  I:integer;
  k:integer;
  c:PComponentName;
  n:string;
begin
  k := 0;
  for I := 0 to length(name) - 1 do
  begin
    if ischaralpha(name[i]) then
    begin
      inc(k);
    end;
  end;
  n := copy(name, 0, k);
  New(c);
  if n = 'Rez' then
    c.cmType := cmRez
  else if n = 'Vol' then
    c.cmType := cmVol
  else if n = 'Amp' then
    c.cmType := cmAmp
  else if n = 'Pow' then
    c.cmType := cmPow
  else if n = 'Lamp' then
    c.cmType := cmLamp
  else if n = 'Key' then
    c.cmType := cmKey
  else
    c.cmType := cmLogElement;

  c.n := strtoint(copy(name, k+1, length(name) - k));
  result := c;
end;

function TMDIChild.getNameFromType(cmType: TComponentType; n: Integer):string;
begin
case cmType of
    cmLamp:
    begin
      result := 'Lamp' + inttostr(n);
    end;
    cmKey:
    begin
      result := 'Key' + inttostr(n);
    end;
    cmRez:
    begin
      result := 'Rez' + inttostr(n);
    end;
    cmAmp:
    begin
      result := 'Amp' + inttostr(n);
    end;
    cmVol:
    begin
      result := 'Vol' + inttostr(n);
    end;
    cmPow:
    begin
      result := 'Pow' + inttostr(n);
    end
    else
    begin
      result := 'LogElement' + inttostr(n);
    end;
  end;
end;

procedure TMDIChild.addComponent(cmType:TComponentType);
begin
  createComponent(
    self.left + 40,
    self.Top + 40,
    cmType,
    cnt
  );
  inc(cnt);
end;

procedure TMDIChild.FormClick(Sender: TObject);
begin
  if moving then
  begin
    moving := false;
    unselectAllComponents;
    invalidate;
  end;
end;

procedure TMDIChild.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TMDIChild.FormCreate(Sender: TObject);
begin
  Sent := 0;
  step := 0;
  cnt := 1;
  shifting := false;
  List := TList.Create;
  eventList := TList.Create;
end;

procedure TMDIChild.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if moving then
  begin
//    if (touchComponent(x, y)) then
      invalidate;
    moveX := x;
    moveY := y;    
  end;
end;

procedure TMDIChild.FormPaint(Sender: TObject);
var
  p: PConnection;
  i:integer;
  p1, p2, p3:PPoint;
  listPoints: TList;
  arr:TMass;
  comp1Points, comp2Points: PMass;
  comp1, comp2: TLogElement;
  e:TEvent;
  s1, s2:string;
begin
  New(comp1Points);
  New(comp2Points);
  listPoints := TList.Create;
  New(p1);
  New(p2);
  New(p3);
  New(arr[0]);
  New(arr[1]);
  New(arr[2]);
  for i := 0 to list.Count - 1 do
  begin
    p := PConnection(list.items[i]);
    comp1 := FindComponent(p.Name1) as TLogElement;
    comp2 := FindComponent(p.Name2) as TLogElement;
    comp1Points := self.getCoordComp(comp1);
    comp2Points := self.getCoordComp(comp2);
    if (comp1 <> Nil) and (comp2 <> Nil) then
    begin
      if (comp1.left + comp1.width < comp2.left) then
      begin
        arr := calcPoints(listPoints, comp1Points, comp2Points, 2);
      end
      else if (comp2.left + comp2.width < comp1.left) then
      begin
        arr := calcPoints(listPoints, comp1Points, comp2Points, 0);
      end
      else
      begin
        if (comp1.Top + comp1.Height < comp2.top) then
            arr := calcPoints(listPoints, comp1Points, comp2Points, 3)
        else if (comp2.Top + comp2.Height < comp1.top) then
            arr := calcPoints(listPoints, comp1Points, comp2Points, -1)
      end;
      p1 := arr[0];
      p2 := arr[1];
      p3 := arr[2];
      listPoints.Add(p1);
      listPoints.Add(p3);
    end;

    if p1.X <> -1 then
      canvas.MoveTo(p1.X, p1.y);
    if p2.X <> -1 then
      canvas.lineTo(p2.X, p2.y);
    if p3.X <> -1 then
      canvas.lineTo(p3.X, p3.y);
  end;

  if moving and touchComponent(movex, movey) then
  begin
      canvas.MoveTo(startX, startY);
      canvas.LineTo(moveX, startY);
      canvas.LineTo(moveX, moveY);
  end;
end;

procedure TMDIChild.LogElement1Click(Sender: TObject);
var
  CompName:string;
  p:PConnection;
  log: TLogElement;
begin
  log := (sender as TLogElement);
  moving := not moving;
  if moving then
  begin
    New(p);
    startX := log.left + log.Width div 2;
    startY := log.top + log.Height div 2;
    p.Name1 := log.Name;
    movingLink := p;
    log.selected := true;
  end
  else
  begin
    (FindComponent(movingLink.name1) as TLogElement).selected := false;
    movingLink.Name2 := log.Name;
    createConnection(
      movingLink.Name1,
      movingLink.Name2
    );
    invalidate;
  end;
end;

procedure TMDIChild.LogElement1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if button = mbLeft then
begin
  shiftComp := (sender as TLogElement);
  startX := x;
  startY := y;
  shifting := true;
end
else if button = mbRight then
begin
  shiftComp := (sender as TLogElement);
end;
end;

procedure TMDIChild.LogElement1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  i:integer;
  helpX, helpY:boolean;
  comp:TComponent;
  log :TLogElement;
  curX, curY:integer;
begin
  if shifting then
  begin
    helpX := false;
    helpY := false;
//    curX := mouse.CursorPos.X-self.Left-5 - startX;
//    curY := mouse.CursorPos.Y-self.top-30 - startY;
    log := (sender as TLogElement);
    curX:=log.left + x - startX;
    curY:=log.Top + y - startY;
    for I := 0 to ComponentCount - 1 do
    begin
      comp := components[i];
      if comp is TLogElement then
      begin
        log := (comp as TLogElement);
        if (log.name <> shiftComp.Name) then
        begin
          if
          (log.Left - 10 < curx) and
          (log.Left + 10 > curx)
          then
          begin
            shiftComp.Left := log.Left;
            helpX := true;
          end;
          if
          (log.top - 10 < cury) and
          (log.top + 10 > cury)
          then
          begin
            shiftComp.top := log.top;
            helpY := true;
          end;

        end;
      end;
    end;
    if not helpX then
      shiftComp.left:=curX;
    if not helpY then
      shiftComp.top:=curY;
    didShift := true;
    invalidate;
  end;
  if moving then
  begin
    log := sender as TLogElement;
    moveX := log.Left + x;
    moveY := log.top + Y;
    invalidate;
  end;
end;

procedure TMDIChild.LogElement1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  log: TLogElement;
  p : PComponentName;
begin
if button = mbLeft then
begin
  if (((Sender as TLogElement).Name = movingLink.Name1) and didShift) then
  begin
    moving := false;
    unselectAllComponents;
    log := (Sender as TLogElement);
    log.selected := false;
    p := getTypeFromName(log.name);
    moveComponent(log.Left, log.top, p.cmType, p.n);
  end;
  shiftComp := Nil;
  didShift := false;
  shifting := false;
end;
end;

procedure TMDIChild.createConnection(comp1Name, comp2Name: string);
begin
  createConnection(comp1Name, comp2Name, false);
end;

procedure TMDIChild.createConnection(comp1Name, comp2Name: string; withOutLog:boolean);
var
  p:PConnection;
begin
  New(p);
  p.Name1 := comp1Name;
  p.Name2 := comp2Name;
  list.Add(p);
  if (not withOutLog) then
  begin
    clearAfterEventList(step);
    TEvent.CreateConnection(eventList, p.Name1, p.Name2);
    step := eventList.Count - 1;
  end;
//  updateConnectionsList;
//  updateEventList;
end;

procedure TMDIChild.Delete2Click(Sender: TObject);
var
  p:PComponentName;
begin
  p := getTypeFromName(shiftComp.Name);
  shiftComp := Nil;
  deleteComponent(p.cmType, p.n);
  invalidate;
end;

procedure TMDIChild.createComponent(left, top: integer; compType:TComponentType; n:integer; withOutLog:boolean);
var
  p:PConnection;
  name:string;
  log:TLogElement;
begin
  case compType of
    cmLamp:
    begin
      log := TLamp.Create(self);
      name := 'Lamp';
    end;
    cmKey:
    begin
      log := TKey.Create(self);
      name := 'Key';
    end;
    cmRez:
    begin
      log := TResistor.Create(self);
      name := 'Rez';
    end;
    cmAmp:
    begin
      log := TAmpermeter.Create(self);
      name := 'Amp';
    end;
    cmVol:
    begin
      log := TVoltmeter.Create(self);
      name := 'Vol';
    end;
//    cmNode:
//    begin
//      log := TNode.Create(self);
//      name := 'Node';
//    end;
    cmPow:
    begin
      log := TBattery.Create(self);
      name := 'Pow';
    end
    else
    begin
      log := TLogElement.Create(self);
      name := 'LogElement';
    end;
  end;
  log.Parent := self;
  log.Left := left;
  log.top := top;
  log.Name := name + inttostr(n);
  log.OnClick := LogElement1Click;
  log.OnMouseUp := LogElement1MouseUp;
  log.OnMouseDown := LogElement1MouseDown;
  log.OnMouseMove := LogElement1MouseMove;
  log.PopupMenu := compMenu;
  if (not withOutLog) then
  begin
    clearAfterEventList(step);
    TEvent.CreateComponent(eventList, compType, n, Left, top);
    step := eventList.Count - 1;
  end;
//  updateEventList;
end;

procedure TMDIChild.createComponent(left, top: integer; compType:TComponentType;  n:integer);
begin
  createComponent(left, top, compType, n, false);
end;

procedure TMDIChild.moveComponent(left, top: integer; cmType: TComponentType; n:integer; withOutLog:boolean);
var
  p:PConnection;
  log:TLogElement;
begin
  log := (findcomponent(getNameFromType(cmType, n)) as TLogElement);
  if log <> Nil then
  begin
    log.Left := left;
    log.top := top;
    if (not withOutLog) then
    begin
      clearAfterEventList(step);
      TEvent.MoveComponent(eventList, cmType, n, Left, top);
      step := eventList.Count - 1;
    end;
//    updateEventList;
  end;
end;

procedure TMDIChild.moveComponent(left, top: integer; cmType:TComponentType; n:integer);
begin
  moveComponent(left, top, cmType, n, false);
end;

procedure TMDIChild.deleteComponent(cmType: TComponentType; n:integer; withOutLog: Boolean);
var
  log:TLogElement;
  i:integer;
  p:PConnection;
begin
  log := (findcomponent(getNameFromType(cmType, n)) as TLogElement);
  if (log <> Nil) then
  begin
    for I := list.Count - 1 downto 0 do
    begin
      p := PConnection(list[i]);
      if (p.Name1 = log.Name) or (p.Name2 = log.Name) then
      begin
        list.Delete(i);
      end;
    end;

    if (not withOutLog) then
    begin
      clearAfterEventList(step);
      TEvent.DeleteComponent(eventList, cmType, n);
      step := eventList.Count - 1;
    end;
    log.Destroy;
//    updateEventList;
  end;
end;

procedure TMDIChild.deleteComponent(cmType: TComponentType; n:integer);
begin
  deleteComponent(cmType, n, false);
end;

procedure TMDIChild.deleteConnection(name1, name2: string; withOutLog: Boolean);
var
  log:TLogElement;
  i:integer;
  p:PConnection;
begin
  for I := 0 to list.Count - 1 do
  begin
    p := PConnection(list[i]);
    if (p.Name1 = name1) and (p.Name2 = name2) then
    begin
      list.Delete(i);
      if (not withOutLog) then
      begin
        clearAfterEventList(step);
        TEvent.DeleteConnection(eventList, Name1, name2);
        step := eventList.Count - 1;
      end;
//      updateConnectionsList;
//      updateEventList;
      exit;
    end;
  end;
end;

procedure TMDIChild.deleteConnection(name1, name2:string);
begin
  deleteConnection(name1, name2, false);
end;

procedure TMDIChild.turnComponent(cmType: TComponentType; n: Integer;
  withOutLog: Boolean);
var
  log:TLogElement;
  i:integer;
  p:PConnection;
begin
  log := (findcomponent(getNameFromType(cmType, n)) as TLogElement);
  if (log <> Nil) then
  begin
    if (not withOutLog) then
    begin
      clearAfterEventList(step);
      TEvent.TurnComponent(eventList, cmType, n);
      step := eventList.Count - 1;
    end;
    log.turn;
//    updateEventList;
  end;
end;

procedure TMDIChild.turnComponent(cmType: TComponentType; n: Integer);
begin
  turnComponent(cmType, n, false);
end;

procedure TMDIChild.clearAfterEventList(n:integer);
var i:integer;
begin
  for I := eventList.Count - 1 downto n + 1 do
  begin
    eventList.delete(i);
  end;
end;

procedure TMDIChild.toEvent(id: Integer);
var
  i, j:integer;
  E:TEvent;
  log : TLogElement;
  p:PConnection;
begin
  list.Clear;
  for I := ComponentCount - 1  downto 0 do
  begin
    if (Components[i] is TLogElement) then
      Components[i].Destroy;
  end;

  for i := 0 to id do
  begin
    e := eventList[i];
    if e.typeEvent = evCreate then
    begin
      if e.data.typeData = dtComponent then
      begin
        createComponent(
          e.data.coord.X,
          e.data.coord.Y,
          e.data.componenType,
          e.data.n,
          true
        );
      end
      else
      begin
        createConnection(
          e.data.connectionComponentName1,
          e.data.connectionComponentName2,
          true
        );
      end;
    end
    else if e.typeEvent = evMove then
    begin
      moveComponent(
        e.data.coord.X,
        e.data.coord.y,
        e.data.componenType,
        e.data.n,
        true
      );
    end
    else if e.typeEvent = evDelete then
    begin
      if e.data.typeData = dtComponent then
        deleteComponent(e.data.componenType, e.data.n, true)
      else
      begin
        deleteConnection(
          e.data.connectionComponentName1,
          e.data.connectionComponentName2,
          true
        );
      end;
    end
    else if e.typeEvent = evTurn then
    begin
      if e.data.typeData = dtComponent then
        turnComponent(e.data.componenType, e.data.n, true);
    end;
  end;
//  updateConnectionsList;
//  updateEventList;
  step := id;
  invalidate;
end;


end.
