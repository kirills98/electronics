unit Event;

interface

uses Windows, Classes, SysUtils, Dialogs;

type
  TTypeEvent = (evMove, evCreate, evDelete, evTurn);
  TTypeData = (dtComponent, dtConnection);
  
  TComponentType = (
    cmKey,
    cmLamp,
    cmPow,
    cmAmp,
    cmVol,
    cmRez,
    cmNone,
    cmNode,
    cmLogElement
  );

  TData = record
    typeData: TTypeData;
    componenType: TComponentType;
    n:integer;
    connectionComponentName1, connectionComponentName2:string;
    coord: TPoint;
  end;


  PEvent = ^TEvent;
  TEvent = class(TObject)
    id:integer;
    timestamp: TTimeStamp;
    typeEvent: TTypeEvent;
    data: TData;
    constructor Create; overload;
    constructor Create(list:TList; typeEvent:TTypeEvent; typeData: TTypeData;
      componentType:TComponentType; n:integer;
      connectionComponent1Name, connectionComponent2Name :string;
      x, y:integer);overload;
    constructor CreateComponent(list:TList; componentType:TComponentType; n:integer; x, y:integer);
    constructor CreateConnection(list:TList; component1Name, component2Name:string);
    constructor MoveComponent(list:TList; componentType:TComponentType; n:integer; x, y:integer);
    constructor DeleteComponent(list:TList; componentType:TComponentType; n:integer);
    constructor TurnComponent(list:TList; componentType:TComponentType; n:integer);
    constructor DeleteConnection(list:TList; component1Name, component2Name:string);
    procedure saveToStream;
    function toString:string;
    function getComponentName:string;
  end;

implementation

constructor TEvent.Create;
begin
  self.id := 0;
  self.timestamp := DateTimeToTimeStamp(Now);
  self.typeEvent := evCreate;
  self.data.typeData := dtComponent;
  self.data.componenType := cmNone;
  self.data.n := 0;
  self.data.connectionComponentName1 := '';
  self.data.connectionComponentName2 := '';
  self.data.coord.X := 0;
  self.data.coord.Y := 0;
end;

constructor TEvent.Create(
  list:TList;
  typeEvent:TTypeEvent;
  typeData: TTypeData;
  componentType:TComponentType;
  n:integer;
  connectionComponent1Name, connectionComponent2Name :string;
  x, y:integer
);
begin
  self.id := list.Count;
  self.timestamp := DateTimeToTimeStamp(Now);
  self.typeEvent := typeEvent;
  self.data.typeData := typeData;
  self.data.componenType := componentType;
  self.data.n := n;
  self.data.connectionComponentName1 := connectionComponent1Name;
  self.data.connectionComponentName2 := connectionComponent2Name;
  self.data.coord.X := x;
  self.data.coord.Y := Y;
  list.Add(self);
//  saveToStream;
end;

constructor TEvent.CreateComponent(list:TList; componentType:TComponentType; n:integer; x, y:integer);
begin
  Create(
    list,
    evCreate,
    dtComponent,
    componentType,
    n,
    '','',
    x,y
  );
end;

constructor TEvent.CreateConnection(list:TList; component1Name, component2Name:string);
begin
  create(
    list,
    evCreate,
    dtConnection,
    cmNone,
    -1,
    component1Name,
    component2Name,
    -1,-1
  );
end;

constructor TEvent.MoveComponent(list:TList; componentType:TComponentType; n:integer; x, y:integer);
begin
  create(
    list,
    evMove,
    dtComponent,
    componentType,
    n,
    '','',
    x,y
  );
end;

constructor TEvent.DeleteComponent(list:TList; componentType:TComponentType; n:integer);
begin
  create(
    list,
    evDelete,
    dtComponent,
    componentType,
    n,
    '', '',
    -1,-1
  );
end;

constructor TEvent.TurnComponent(list:TList; componentType:TComponentType; n:integer);
begin
  create(
    list,
    evTurn,
    dtComponent,
    componentType,
    n,
    '', '',
    -1,-1
  );
end;

constructor TEvent.DeleteConnection(list:TList; component1Name, component2Name:string);
begin
  create(
    list,
    evDelete,
    dtComponent,
    cmNone,
    -1,
    component1Name,
    component2Name,
    -1, -1
  );
end;

procedure TEvent.saveToStream;
var
  stream:TMemoryStream;
  e:TEvent;
  s : string;
begin
  stream := TMemoryStream.Create;
  stream.Write(self, self.InstanceSize);
  stream.SaveToFile('test.str');
end;

function TEvent.toString;
var
  s1, s2, res:string;
begin
  if (typeEvent = evMove) then
    s1 := 'move'
  else if (typeEvent = evCreate) then
    s1 := 'create'
  else if (typeEvent = evDelete) then
    s1 := 'del'
  else
    s1 := 'turn'; 

  if (data.typeData = dtComponent) then
    s2 := 'comp'
  else
    s2 := 'conn';
  res :=
     inttostr(id) + ' : ' +
     s1 + ' : ' +
     datetimetostr(timestamptodatetime(timestamp)) + ' : ' +
     getComponentName + ' : ' +
     data.connectionComponentName1 + ' : ' +
     data.connectionComponentName2 + ' : ' +
     s2 + ' : ' +
     inttostr(data.coord.x) + ' : ' +
     inttostr(data.coord.y) + ' : ';
  result := res;
end;

function TEvent.getComponentName:string;
begin
case data.componenType of
    cmLamp:
    begin
      result := 'Lamp' + inttostr(data.n);
    end;
    cmKey:
    begin
      result := 'Key' + inttostr(data.n);
    end;
    cmRez:
    begin
      result := 'Rez' + inttostr(data.n);
    end;
    cmAmp:
    begin
      result := 'Amp' + inttostr(data.n);
    end;
    cmVol:
    begin
      result := 'Vol' + inttostr(data.n);
    end;
//    cmNode:
//    begin
//      result := 'Node' + inttostr(data.n);
//    end;
    cmPow:
    begin
      result := 'Pow' + inttostr(data.n);
    end
    else
    begin
      result := 'LogElement' + inttostr(data.n);
    end;
  end;
end;

end.
