unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LogElements, Menus, StdCtrls, ComCtrls, ToolWin, Event, ExtCtrls;

type
  TMass = array[0..3] of PPoint;
  PMass = ^TMass;
  TConnectionType = (CtInput, CtOutput);
  PConnection = ^TConnection;
  TConnection = Record
    Name1, Name2:string;
    ConnectionType:TConnectionType;
  End;
  TForm1 = class(TForm)
    ListBox1: TListBox;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    Button2: TButton;
    ListBox2: TListBox;
    Button1: TButton;
    Button3: TButton;
    Splitter1: TSplitter;
    Button4: TButton;
    procedure FormCreate(Sender: TObject);
    procedure LogElement1Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure LogElement1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LogElement1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button2Click(Sender: TObject);
    procedure LogElement1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormClick(Sender: TObject);
    procedure ListBox2DblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    Sent:integer;
    List, eventList:TList;
    function calcPoints(listPoints: TList; comp1Points, comp2Points: PMass; n:integer) : Tmass;
    function getCoordComp(comp : TLogElement):PMass;
    function checkPoint(listPoints: TList; p: PPoint): boolean;
    procedure toEvent(id:integer);
    procedure updateConnectionsList();
    procedure updateEventList;
    procedure clearAfterEventList(n:integer);
    procedure createConnection(comp1Name, comp2Name: string; withOutLog: boolean); overload;
    procedure createConnection(comp1Name, comp2Name: string); overload;
    procedure createComponent(left, top: integer; name:string; withOutLog:boolean); overload;
    procedure createComponent(left, top: integer; name:string);overload;
    procedure moveComponent(left, top: integer; name:string; withOutLog:boolean); overload;
    procedure moveComponent(left, top: integer; name:string);overload;
    procedure deleteComponent(name:string; withOutLog:boolean); overload;
    procedure deleteComponent(name:string);overload;
    procedure deleteConnection(name1, name2:string; withOutLog:boolean); overload;
    procedure deleteConnection(name1, name2:string);   overload;
    function touchComponent(x, y:integer):boolean;

  public
    { Public declarations }
    shiftComp: TLogElement;
  end;

var
  Form1: TForm1;
  cnt, startX, startY, moveX, moveY, step: integer;
  moving, shifting, didShift: boolean;
  movingLink: PConnection;

implementation

{$R *.dfm}

procedure TForm1.ToolButton1Click(Sender: TObject);
begin
  createComponent(
    listbox1.left + listbox1.width + 10,
    30,
    'LogElement' + inttostr(cnt)
  );
  inc(cnt);
end;

procedure TForm1.FormClick(Sender: TObject);
begin
  if moving then
  begin
    moving := false;
    invalidate;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Sent := 0;
  step := 0;
  cnt := 1;
  shifting := false;
  List := TList.Create;
  eventList := TList.Create;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if moving then
  begin
    invalidate;
    moveX := x;
    moveY := y;
  end;
end;

function TForm1.getCoordComp(comp: TLogElement) : PMass;
var
  arr : PMass;
begin
    New(arr);
    New(arr[0]);
    New(arr[1]);
    New(arr[2]);
    New(arr[3]);
    arr[0].X := comp.Left;
    arr[0].Y := comp.Top + comp.Height div 2;
    arr[1].X := comp.Left + comp.Width div 2;
    arr[1].Y := comp.top;
    arr[2].X := comp.Left + comp.Width;
    arr[2].Y := arr[0].Y;
    arr[3].X := arr[1].X;
    arr[3].y := comp.Top + comp.Height;
    result := arr;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  step := step - 1;
  toEvent(step);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  n : Integer;
  p : PConnection;
begin
  n := ListBox1.ItemIndex;
  p := PConnection(list[n]);
  deleteConnection(p.Name1, p.Name2);
  Invalidate;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  step := step + 1;
  toEvent(step);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  if moving then
  begin
    deleteComponent(movingLink.Name1);
    moving := false;
    invalidate;
  end;
end;

function TForm1.checkPoint(listPoints: TList; p: PPoint) : boolean;
var
  i:integer;
  point :PPoint;
begin
  for i := 0 to listPoints.Count - 1 do
  begin
    point := PPoint(listPoints.items[i]);
    if ((point.x = p.X) and (point.y = p.y)) then
    begin
      result := false;
      exit;
    end;
  end;
  Result := true;
end;

function TForm1.calcPoints(listPoints: TList;
 comp1Points: PMass; comp2Points: PMass; n:integer): TMass;
var
  arr: TMass;
  p1, p2, p3: PPoint;
begin
  New(p1);
  New(arr[0]);
  New(arr[1]);
  New(arr[2]);
  New(p2);
  New(p3);
  if (checkPoint(listPoints, comp1Points[abs(n)])) then
  begin
    p1 := comp1Points[abs(n)];
    if (comp1Points[3].Y < comp2POints[1].Y) then
      p3 := comp2Points[1]
    else if (comp1Points[1].Y > comp2POints[3].Y) then
      p3 := comp2Points[3]
    else
    begin
      p3 := comp2Points[abs(n-2)];
    end;
    p2.X := p3.X;
    p2.Y := p1.Y;
  end
  else
  begin
    p3 := comp2Points[abs(n-2)];
    if (comp1Points[3].Y < comp2POints[1].Y) then
      p1 := comp1Points[3]
    else if (comp1Points[1].Y > comp2POints[3].Y) then
      p1 := comp1Points[1]
    else
    begin
      p1:= comp2Points[abs(n)];
    end;
    p2.X := p1.X;
    p2.Y := p3.Y;
  end;

  arr[0] := p1;
  arr[1] := p2;
  arr[2] := p3;
  result := arr;
end;

procedure TForm1.FormPaint(Sender: TObject);
var
  p: PConnection;
  i:integer;
  p1, p2, p3:PPoint;
  listPoints: TList;
  arr:TMass;
  comp1Points, comp2Points: PMass;
  comp1, comp2: TLogElement;
  e:TEvent;
  s1, s2:string;
begin
  New(comp1Points);
  New(comp2Points);
  listPoints := TList.Create;
  New(p1);
  New(p2);
  New(p3);
  New(arr[0]);
  New(arr[1]);
  New(arr[2]);
  for i := 0 to list.Count - 1 do
  begin
    p := PConnection(list.items[i]);
    comp1 := FindComponent(p.Name1) as TLogElement;
    comp2 := FindComponent(p.Name2) as TLogElement;
    comp1Points := self.getCoordComp(comp1);
    comp2Points := self.getCoordComp(comp2);
    if (comp1 <> Nil) and (comp2 <> Nil) then
    begin
      if (comp1.left + comp1.width < comp2.left) then
      begin
        arr := calcPoints(listPoints, comp1Points, comp2Points, 2);
      end
      else if (comp2.left + comp2.width < comp1.left) then
      begin
        arr := calcPoints(listPoints, comp1Points, comp2Points, 0);
      end
      else
      begin
        if (comp1.Top + comp1.Height < comp2.top) then
            arr := calcPoints(listPoints, comp1Points, comp2Points, 3)
        else if (comp2.Top + comp2.Height < comp1.top) then
            arr := calcPoints(listPoints, comp1Points, comp2Points, -1)
      end;
      p1 := arr[0];
      p2 := arr[1];
      p3 := arr[2];
      listPoints.Add(p1);
      listPoints.Add(p3);
    end;

    if p1.X <> -1 then
      canvas.MoveTo(p1.X, p1.y);
    if p2.X <> -1 then
      canvas.lineTo(p2.X, p2.y);
    if p3.X <> -1 then
      canvas.lineTo(p3.X, p3.y);
  end;

  if moving and touchComponent(movex, movey) then
  begin
      canvas.MoveTo(startX, startY);
      canvas.LineTo(moveX, startY);
      canvas.LineTo(moveX, moveY);
  end;
end;

function TForm1.touchComponent(x, y:integer):boolean;
var
  i:integer;
  comp:TLogElement;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    if (components[i] is TLogElement) then
    begin
      comp := components[i] as TLogElement;
      if
        (comp.Left <= x) and
        (comp.Left + comp.Width >= x) and
        (comp.top <= y) and
        (comp.Top + comp.Height >= y)
      then
      begin
        result := true;
        exit;
      end;
    end;
  end;
  result := false;
end;

procedure TForm1.updateEventList;
var
  i:integer;
  e:Tevent;
  s1,s2:string;
begin
  ListBox2.Clear;
  for i := 0 to eventList.Count - 1 do
  begin
    e := eventList[i];
    if (e.typeEvent = evMove) then
      s1 := 'move'
    else if (e.typeEvent = evCreate) then
      s1 := 'create'
    else
      s1 := 'del';

    if (e.data.typeData = dtComponent) then
      s2 := 'comp'
    else
      s2 := 'conn';

    ListBox2.Items.Add(
      inttostr(e.id) + ' : ' +
      s1 + ' : ' +
      datetimetostr(timestamptodatetime(e.timestamp)) + ' : ' +
      e.data.componentName + ' : ' +
      e.data.connectionComponentName1 + ' : ' +
      e.data.connectionComponentName2 + ' : ' +
      s2 + ' : ' +
      inttostr(e.data.coord.x) + ' : ' +
      inttostr(e.data.coord.y) + ' : '
    );
    if i = step then
      listbox2.ItemIndex := i;
  end;

  if step < 0 then
    Button1.Enabled := false
  else
    Button1.Enabled := true;

  if step >= eventList.Count - 1 then
    Button3.Enabled := false
  else
    Button3.Enabled := true;
end;

procedure TForm1.updateConnectionsList();
var
  i:integer;
  p:PConnection;
begin
  listbox1.Clear;
  for I := 0 to list.Count - 1 do
  begin
    p := PConnection(list[i]);
    ListBox1.Items.Add(p.Name1 + ' -> ' + p.name2);
  end;
end;

procedure TForm1.ListBox2DblClick(Sender: TObject);
begin
  toEvent(ListBox2.ItemIndex);
end;

procedure TForm1.LogElement1Click(Sender: TObject);
var
  CompName:string;
  p:PConnection;
  log: TLogElement;
begin
  log := (sender as TLogElement);
  moving := not moving;
  if moving then
  begin
    New(p);
    startX := log.left + log.Width div 2;
    startY := log.top + log.Height div 2;
    p.Name1 := log.Name;
    movingLink := p;
    log.selected := true;
  end
  else
  begin
    (FindComponent(movingLink.name1) as TLogElement).selected := false;
    movingLink.Name2 := log.Name;
    createConnection(
      movingLink.Name1,
      movingLink.Name2
    );
    invalidate;
  end;
end;

procedure TForm1.LogElement1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin 
  shiftComp := (sender as TLogElement);
  startX := x;
  startY := y;
  shifting := true;
end;

procedure TForm1.LogElement1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  i:integer;
  helpX, helpY:boolean;
  comp:TComponent;
  log :TLogElement;
  curX, curY:integer;
begin
  if shifting then
  begin
    helpX := false;
    helpY := false;
    curX := mouse.CursorPos.X-form1.Left-5 - startX;
    curY := mouse.CursorPos.Y-form1.top-30 - startY;
    for I := 0 to ComponentCount - 1 do
    begin
      comp := components[i];
      if comp is TLogElement then
      begin
        log := (comp as TLogElement);
        if (log.name <> shiftComp.Name) then
        begin
          if
          (log.Left - 10 < curx) and
          (log.Left + 10 > curx)
          then
          begin
            shiftComp.Left := log.Left;
            helpX := true;
          end;
          if
          (log.top - 10 < cury) and
          (log.top + 10 > cury)
          then
          begin
            shiftComp.top := log.top;
            helpY := true;
          end;

        end;
      end;
    end;
    if not helpX then
      shiftComp.left:=mouse.CursorPos.X-form1.Left-5 - startX;
    if not helpY then
      shiftComp.top:=mouse.CursorPos.y-form1.top-30 - startY;
    didShift := true;
    invalidate;
  end;
  if moving then
  begin
    log := sender as TLogElement;
    moveX := log.Left + x;
    moveY := log.top + Y;
    invalidate;
  end;
end;

procedure TForm1.LogElement1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  log: TLogElement;
begin
  if (((Sender as TLogElement).Name = movingLink.Name1) and didShift) then
  begin
    moving := false;
    log := (Sender as TLogElement);
    log.selected := false;
    moveComponent(log.Left, log.top, log.name);
  end;
  shiftComp := Nil;
  didShift := false;
  shifting := false;
end;

procedure TForm1.createConnection(comp1Name, comp2Name: string);
begin
  createConnection(comp1Name, comp2Name, false);
end;

procedure TForm1.createConnection(comp1Name, comp2Name: string; withOutLog:boolean);
var
  p:PConnection;
begin
  New(p);
  p.Name1 := comp1Name;
  p.Name2 := comp2Name;
  list.Add(p);
  if (not withOutLog) then
  begin
    clearAfterEventList(step);
    TEvent.CreateConnection(eventList, p.Name1, p.Name2);
    step := eventList.Count - 1;
  end;
  updateConnectionsList;
  updateEventList;
end;

procedure TForm1.createComponent(left, top: integer; name:string; withOutLog:boolean);
var
  p:PConnection;
  log:TLogElement;
begin
  log := TLogElement.Create(self);
  log.Parent := self;
  log.Left := left;
  log.top := top;
  log.Name := name;
  log.OnClick := LogElement1Click;
  log.OnMouseUp := LogElement1MouseUp;
  log.OnMouseDown := LogElement1MouseDown;
  log.OnMouseMove := LogElement1MouseMove;
  if (not withOutLog) then
  begin
    clearAfterEventList(step);
    TEvent.CreateComponent(eventList, log.Name, Left, top);
    step := eventList.Count - 1;
  end;
  updateEventList;
end;

procedure TForm1.createComponent(left, top: integer; name:string);
begin
  createComponent(left, top, name, false);
end;

procedure TForm1.moveComponent(left, top: integer; name:string; withOutLog:boolean);
var
  p:PConnection;
  log:TLogElement;
begin
  log := (findcomponent(name) as TLogElement);
  if log <> Nil then
  begin
    log.Left := left;
    log.top := top;
    if (not withOutLog) then
    begin
      clearAfterEventList(step);
      TEvent.MoveComponent(eventList, log.Name, Left, top);
      step := eventList.Count - 1;
    end;
    updateEventList;
  end;
end;

procedure TForm1.moveComponent(left, top: integer; name:string);
begin
  moveComponent(left, top, name, false);
end;

procedure TForm1.deleteComponent(name: string; withOutLog: Boolean);
var
  log:TLogElement;
  i:integer;
  p:PConnection;
begin
  log := (findcomponent(name) as TLogElement);
  if (log <> Nil) then
  begin
    for I := list.Count - 1 downto 0 do
    begin
      p := PConnection(list[i]);
      if (p.Name1 = log.Name) or (p.Name2 = log.Name) then
      begin
        list.Delete(i);
      end;
    end;

    if (not withOutLog) then
    begin
      clearAfterEventList(step);
      TEvent.DeleteComponent(eventList, log.Name);
      step := eventList.Count - 1;
    end;
    log.Destroy;
    updateEventList;
  end;
end;

procedure TForm1.deleteComponent(name:string);
begin
  deleteComponent(name, false);
end;

procedure TForm1.deleteConnection(name1, name2: string; withOutLog: Boolean);
var
  log:TLogElement;
  i:integer;
  p:PConnection;
begin
  for I := 0 to list.Count - 1 do
  begin
    p := PConnection(list[i]);
    if (p.Name1 = name1) and (p.Name2 = name2) then
    begin
      list.Delete(i);
      if (not withOutLog) then
      begin
        clearAfterEventList(step);
        TEvent.DeleteConnection(eventList, Name1, name2);
        step := eventList.Count - 1;
      end;
      updateConnectionsList;
      updateEventList;
      exit;
    end;
  end;
end;

procedure TForm1.deleteConnection(name1, name2:string);
begin
  deleteConnection(name1, name2, false);
end;

procedure TForm1.clearAfterEventList(n:integer);
var i:integer;
begin
  for I := eventList.Count - 1 downto n + 1 do
  begin
    eventList.delete(i);
  end;
end;

procedure TForm1.toEvent(id: Integer);
var
  i, j:integer;
  E:TEvent;
  log : TLogElement;
  p:PConnection;
begin
  list.Clear;
  for I := ComponentCount - 1  downto 0 do
  begin
    if (Components[i] is TLogElement) then
      Components[i].Destroy;
  end;

  for i := 0 to id do
  begin
    e := eventList[i];
    if e.typeEvent = evCreate then
    begin
      if e.data.typeData = dtComponent then
      begin
        createComponent(
          e.data.coord.X,
          e.data.coord.Y,
          e.data.componentName,
          true
        );
      end
      else
      begin
        createConnection(
          e.data.connectionComponentName1,
          e.data.connectionComponentName2,
          true
        );
      end;
    end
    else if e.typeEvent = evMove then
    begin
      moveComponent(
        e.data.coord.X,
        e.data.coord.y,
        e.data.componentName,
        true
      );
    end
    else if e.typeEvent = evDelete then
    begin
      if e.data.typeData = dtComponent then
        deleteComponent(e.data.componentName, true)
      else
      begin
        deleteConnection(
          e.data.connectionComponentName1,
          e.data.connectionComponentName2,
          true
        );
      end;
    end;
  end;
  updateConnectionsList;
  updateEventList;
  step := id;
  invalidate;
end;

end.

