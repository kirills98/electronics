object dmData: TdmData
  OldCreateOrder = False
  Height = 245
  Width = 331
  object IBDatabase1: TIBDatabase
    Connected = True
    DatabaseName = 'C:\Users\14001\Documents\ELECTRONICKS.FDB'
    Params.Strings = (
      'user_name=SysDBA'
      'password=masterkey')
    LoginPrompt = False
    DefaultTransaction = IBTransaction1
    Left = 48
    Top = 72
  end
  object IBTransaction1: TIBTransaction
    Active = True
    DefaultDatabase = IBDatabase1
    Left = 8
    Top = 72
  end
  object IBQuery1: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    SQL.Strings = (
      'select * from LABS where ID_LAB= :ID_LAB')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_LAB'
        ParamType = ptInput
      end>
  end
  object IBQuery2: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    SQL.Strings = (
      'select * from USERS where ID_USER = :ID_USER')
    Left = 48
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_USER'
        ParamType = ptInput
      end>
  end
  object quUpdateEvents: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    SQL.Strings = (
      'Update OPERATIONS '
      'set DATAEVENT = :DATAEVENT '
      'where id_lab = :id_lab'
      'and id_user = :id_user')
    Left = 176
    Top = 8
    ParamData = <
      item
        DataType = ftBlob
        Name = 'DATAEVENT'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'id_lab'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'id_user'
        ParamType = ptInput
      end>
  end
  object quSelectEvents: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    SQL.Strings = (
      
        'SELECt * FROM OPERATIONS where ID_LAB = :id_lab and ID_USER = :i' +
        'd_user')
    Left = 232
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'id_lab'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'id_user'
        ParamType = ptInput
      end>
  end
  object spOperation: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'INSOPERATION'
    Left = 168
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_LAB'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_USER'
        ParamType = ptInput
      end>
  end
end
