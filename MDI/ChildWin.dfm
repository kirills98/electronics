object MDIChild: TMDIChild
  Left = 197
  Top = 117
  Caption = 'MDI Child'
  ClientHeight = 246
  ClientWidth = 351
  Color = clBtnFace
  ParentFont = True
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClick = FormClick
  OnClose = FormClose
  OnCreate = FormCreate
  OnMouseMove = FormMouseMove
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object compMenu: TPopupMenu
    Left = 296
    Top = 200
    object Delete2: TMenuItem
      Caption = 'Delete'
      OnClick = Delete2Click
    end
    object urn1: TMenuItem
      Caption = 'Turn'
      OnClick = urn1Click
    end
  end
end
